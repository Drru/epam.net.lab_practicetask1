﻿using System;

namespace EpamITLab.Task4
{
    public sealed class Matrix
    {
        private readonly double[,] _matrix;
        private int _m; // row
        private int _n; // column

        public int RowsCount
        {
            get { return _m; }
            private set
            {
                if (value > 0)
                {
                    _m = value;
                }
            }
        }

        public int ColumnsCount
        {
            get { return _n; }
            private set
            {
                if (value > 0)
                {
                    _n = value;
                }
            }
        }

        public double this[int row, int column]
        {
            get { return _matrix[row, column]; }
            set
            {
                if (column >= ColumnsCount || row >= RowsCount)
                {
                    throw new ArgumentOutOfRangeException($"Index was out of range");
                }

                _matrix[row, column] = value;
            }
        }

        public Matrix()
        {
            RowsCount = 3;
            ColumnsCount = 3;
            _matrix = new double[3, 3];
        }

        public Matrix(int m, int n)
        {
            RowsCount = m;
            ColumnsCount = n;
            _matrix = new double[RowsCount, ColumnsCount];
        }

        public Matrix(double[,] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException(nameof(matrix), "Matrix cannot be null");
            }

            RowsCount = matrix.GetLength(0);
            ColumnsCount = matrix.GetLength(1);
            _matrix = matrix;
        }

        public Matrix(Matrix matrix) : this(matrix._matrix) { }

        private static double Determinant(double[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new ArgumentException("Rows number not equal columns number");
            }

            double det = 0;
            var rank = matrix.GetLength(0);

            if (rank == 1)
            {
                det = matrix[0, 0];
            }
            if (rank == 2)
            {
                det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
                
            }
            if (rank > 2)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    det += Math.Pow(-1, 0 + j) * matrix[0, j] * Determinant(GetMinor(matrix, 0, j));
                }
            }

            return det;
        }

        private static double[,] GetMinor(double[,] matrix, int row, int column)
        {

            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new ArgumentException("Rows number not equal columns number");
            }

            var buffer = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (i != row || j != column)
                    {
                        if (i > row && j < column) buffer[i - 1, j] = matrix[i, j];
                        if (i < row && j > column) buffer[i, j - 1] = matrix[i, j];
                        if (i > row && j > column) buffer[i - 1, j - 1] = matrix[i, j];
                        if (i < row && j < column) buffer[i, j] = matrix[i, j];
                    }
                }
            }

            return buffer;
        }

        public static double KthMinor(Matrix matrix, int k)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException(nameof(matrix), "Matrix cannot be null");
            }

            if (k < 0 || k >= matrix.RowsCount)
            {
                throw new ArgumentException("Minor order must be in range from 0 to matrix size", nameof(k));
            }

            return Determinant(GetMinor(matrix._matrix, k, k));
        }

        public double KthMinor(int k)
        {
            return KthMinor(this, k);
        }

        public static double RowColumnMinor(Matrix matrix, int i, int j)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException(nameof(matrix), "Matrix cannot be null");
            }

            if (i < 0 || i >= matrix.RowsCount || j < 0 || j >= matrix.ColumnsCount)
            {
                throw new ArgumentException("Minor order must be in range from 0 to matrix size");
            }

            return Determinant(GetMinor(matrix._matrix, i, j));
        }

        public double RowColumnMinor(int i, int j)
        {
            return RowColumnMinor(this, i, j);
        }

        public static Matrix operator +(Matrix lhs, Matrix rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Matrix cannot be null");
            }

            if (lhs.RowsCount != rhs.RowsCount || lhs.ColumnsCount != rhs.ColumnsCount)
            {
                throw new ArgumentException("Matrices must be the same size");
            }

            var m = lhs.RowsCount;
            var n = lhs.ColumnsCount;
            var sum = new double[m, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    sum[i, j] = lhs[i, j] + rhs[i, j];
                }
            }

            return new Matrix(sum);
        }

        public static Matrix Add(Matrix lhs, Matrix rhs)
        {
            return lhs + rhs;
        }

        public Matrix Add(Matrix matrix)
        {
            return this + matrix;
        }

        public static Matrix operator -(Matrix lhs, Matrix rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Matrix cannot be null");
            }

            if (lhs.RowsCount != rhs.RowsCount || lhs.ColumnsCount != rhs.ColumnsCount)
            {
                throw new ArgumentException("Matrices must be the same size");
            }

            var m = lhs.RowsCount;
            var n = lhs.ColumnsCount;
            var subtraction = new double[m, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    subtraction[i, j] = lhs[i, j] - rhs[i, j];
                }
            }

            return new Matrix(subtraction);
        }

        public static Matrix Subtract(Matrix lhs, Matrix rhs)
        {
            return lhs - rhs;
        }

        public Matrix Subtract(Matrix matrix)
        {
            return this - matrix;
        }

        public static Matrix operator *(Matrix lhs, Matrix rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Matrix cannot be null");
            }

            if (lhs.ColumnsCount != rhs.RowsCount)
            {
                throw new ArgumentException("Rows count in first matrix must be equal columns count in second");
            }

            var x = lhs.RowsCount;
            var y = rhs.ColumnsCount;
            var z = rhs.RowsCount;

            var multiplication = new double[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    for (int k = 0; k < z; k++)
                    {
                        multiplication[i, j] += lhs[i, k] * rhs[k, j];
                    }
                }
            }

            return new Matrix(multiplication);
        }

        public static Matrix Multiply(Matrix lhs, Matrix rhs)
        {
            return lhs * rhs;
        }

        public Matrix Multiply(Matrix matrix)
        {
            return this * matrix;
        }

        public override string ToString()
        {
            var result = string.Empty;

            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    result += this[i, j] + "\t";
                }
                result += "\n";
            }

            return result;
        }
    }
}
