﻿using EpamITLab.Task1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EpamITLab.UnitTests
{
    [TestClass]
    public class VectorTests
    {
        [TestMethod]
        public void CanCreateEmptyVector()
        {
            var vector = new Vector();

            Assert.AreEqual(4, vector.Length);
        }

        [TestMethod]
        public void CanCreateVectorFromExistingArray()
        {
            var array = new[] {5, 25, 625};
            var vector = new Vector(array);

            
            Assert.AreEqual(array.Length, vector.Length);
            Assert.AreEqual(array[0], vector[0]);
            Assert.AreEqual(array[1], vector[1]);
            Assert.AreEqual(array[2], vector[2]);
        }

        [TestMethod]
        public void CanCreateVectorFromExistingArrayWithSpecifiedStartIndex()
        {
            var array = new[] {5, 25, 625};
            const int startIndex = 2;
            var vector = new Vector(array, startIndex, 10);

            Assert.AreEqual(array[0], vector[0 + startIndex]);
            Assert.AreEqual(array[1], vector[1 + startIndex]);
            Assert.AreEqual(array[2], vector[2 + startIndex]);
        }

        [TestMethod]
        public void CanAddTwoVectors()
        {
            var lhs = new Vector(new[] {2, 3, 5});
            var rhs = new Vector(new[] {3, 4, 6});
            var result = lhs + rhs;

            Assert.IsNotNull(result);
            Assert.AreEqual(5, result[0]);
            Assert.AreEqual(7, result[1]);
            Assert.AreEqual(11, result[2]);
        }

        [TestMethod]
        public void CanSubtractTwoVectors()
        {
            var lhs = new Vector(new[] { 2, 3, 5 });
            var rhs = new Vector(new[] { 3, 4, 6 });
            var result = lhs - rhs;

            Assert.IsNotNull(result);
            Assert.AreEqual(-1, result[0]);
            Assert.AreEqual(-1, result[1]);
            Assert.AreEqual(-1, result[2]);
        }

        [TestMethod]
        public void CanMultiplyVectorOnScalar()
        {
            var vector = new Vector(new[] {5, -7, 9});
            const int scalar = -3;
            var result = vector * scalar;

            Assert.IsNotNull(result);
            Assert.AreEqual(-15, result[0]);
            Assert.AreEqual(21, result[1]);
            Assert.AreEqual(-27, result[2]);
        }
    }
}
