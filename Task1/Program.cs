﻿using System;

namespace EpamITLab.Task1
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Creating vector 1");
            var vector1 = new Vector(new[] {1, 2, 3, 4, 5});
            Console.WriteLine("Vector 1 contains:");
            Console.WriteLine(vector1);

            Console.WriteLine("Creating vector 2");
            var vector2 = new Vector(new[] {5, 4, 3, 2, 1});
            Console.WriteLine("Vector 2 contains:");
            Console.WriteLine(vector2);

            Console.WriteLine("Calculate sum of vector 1 and vector 2");
            var sum = vector1 + vector2;
            Console.WriteLine($"vector1 + vector2 = {sum}");

            Console.WriteLine("Calculate subtraction of vector 1 and vector 2");
            var subtr = vector1 - vector2;
            Console.WriteLine($"vector1 - vector2 = {subtr}");

            const int scalar = 3;
            Console.WriteLine($"Calculate multiplication vector 1 on {scalar}");
            var scalarMult =  vector1 * scalar;
            Console.WriteLine($"vector1 * {scalar} = {scalarMult}");

            Console.WriteLine("Checking equality of vector 1 and vector 2");
            Console.WriteLine($"vector1 = vector2: {vector1 == vector2}");

            Console.WriteLine("Creating new vector 3 from vector 2 in range from 3 and length 8");
            var vector3 = new Vector(vector2, 3, 8);

            Console.WriteLine("Vector 3 contains:");
            Console.WriteLine(vector3);

            Console.ReadLine();
        }
    }
}
