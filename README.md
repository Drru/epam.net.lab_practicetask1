EPAM IT LAB .NET Practice Task 1

Written on C# 6.0 using .NET Framework 4.6

Solution consists of 5 projects:
* Task1 : Vector;
* Task2 : Rectangle;
* Task3 : Polynomial;
* Task4 : Matrix;
* UnitTests