﻿using System;
using System.Drawing;

namespace EpamITLab.Task2
{
    public sealed class Rectangle
    {
        private readonly Point[] _point = new Point[4];
        private int _width;
        private int _height;

        public Point this[int index] => _point[index];

        public int X => this[0].X;

        public int Y => this[0].Y;

        public int Width
        {
            get { return _width; }
            set
            {
                if (value >= 0)
                {
                    _width = value;
                    CalculateCoordinates();
                }
            }
        }

        public int Height
        {
            get { return _height; }
            set
            {
                if (value >= 0)
                {
                    _height = value;
                    CalculateCoordinates();
                }
            }
        }

        public Rectangle() { }

        public Rectangle(int x, int y, int width, int height)
        {
            _point[0] = new Point(x, y);
            Width = width;
            Height = height;

            CalculateCoordinates();
        }

        public Rectangle(Point point, Size size)
        {
            _point[0] = point;
            Width = size.Width;
            Height = size.Height;

            CalculateCoordinates();
        }

        public Rectangle(Rectangle rectangle)
        {
            if (rectangle == null)
            {
                throw new ArgumentNullException(nameof(rectangle), "Rectangle cannot be null");
            }

            _point = rectangle._point;
            Width = rectangle.Width;
            Height = rectangle.Height;
        }

        private void CalculateCoordinates()
        {
            _point[1] = new Point(_point[0].X + Width, _point[0].Y);
            _point[2] = new Point(_point[0].X + Width, _point[0].Y - Height);
            _point[3] = new Point(_point[0].X, _point[0].Y - Height);
        }

        public void MoveTo(int x, int y)
        {
            _point[0] = new Point(x, y);

            CalculateCoordinates();
        }

        public void MoveTo(Point point)
        {
            MoveTo(point.X, point.Y);
        }

        public void Resize(int width, int height)
        {
            Width = width;
            Height = height;

            CalculateCoordinates();
        }

        public void Resize(Size size)
        {
            Resize(size.Width, size.Height);
        }

        public static Rectangle Intersect(Rectangle lhs, Rectangle rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Rectangle cannot be null");
            }

            var x = Math.Max(lhs[0].X, rhs[0].X);
            var num1 = Math.Min(lhs[0].X + lhs.Width, rhs[0].X + rhs.Width);
            var y = Math.Max(lhs[0].Y, rhs[0].Y);
            var num2 = Math.Min(lhs[0].Y + lhs.Height, rhs[0].Y + rhs.Height);

            if (num1 >= x && num2 >= y)
            {
                return new Rectangle(x, y, num1 - x, num2 - y);
            }

            return null;
        }

        public Rectangle Intersect(Rectangle rectangle)
        {
            return Intersect(this, rectangle);
        }

        public static Rectangle Union(Rectangle lhs, Rectangle rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Rectangle cannot be null");
            }

            var x = Math.Min(lhs[0].X, rhs[0].X);
            var num1 = Math.Max(lhs[0].X + lhs.Width, rhs[0].X + rhs.Width);
            var y = Math.Min(lhs[0].Y, rhs[0].Y);
            var num2 = Math.Max(lhs[0].Y + lhs.Height, rhs[0].Y + rhs.Height);

            return new Rectangle(x, y, num1 - x, num2 - y);
        }

        public Rectangle Union(Rectangle rectangle)
        {
            return Union(this, rectangle);
        }

        public override string ToString()
        {
            var result = "( ";
            for (int i = 0; i < _point.Length; i++)
            {
                result += i != 3 ? $"({this[i].X}, {this[i].Y}), " : $"({this[i].X}, {this[i].Y})";
            }
            result += " ); " + $"Width = {Width}; Height = {Height}";

            return result;
        }
    }
}
