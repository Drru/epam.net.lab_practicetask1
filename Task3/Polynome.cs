﻿using System;
using System.Text;

namespace EpamITLab.Task3
{
    public sealed class Polynome
    {
        private readonly double[] _coefficients;
        private int _maxPower;

        public int Power
        {
            get { return _maxPower; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Power cannot be negative", nameof(value));
                }

                _maxPower = value;
            }
        }

        public double this[int index]
        {
            get
            {
                if (index < 0 || index >= _coefficients.Length)
                {
                    throw new ArgumentOutOfRangeException(nameof(index), index, "Index was out of range");
                }

                return _coefficients[index];
            }
            set
            {
                if (index < 0 || index >= _coefficients.Length)
                {
                    throw new ArgumentOutOfRangeException(nameof(index), index, "Index was out of range");
                }

                _coefficients[index] = value;
            }
        }

        public Polynome(int power)
        {
            Power = power;
            _coefficients = new double[Power + 1];
        }

        public Polynome(int power, params double[] coefficients)
        {
            Power = power;
            _coefficients = coefficients;
        }

        public Polynome(params double[] coefficients)
        {
            Power = coefficients.Length - 1;
            _coefficients = coefficients;
        }

        public double CalculateValue(double x)
        {
            double value = 0;

            for (int i = _coefficients.Length - 1; i >= 0; i--)
            {
                value = x * value + _coefficients[i];
            }

            return value;
        }

        public static Polynome operator +(Polynome lhs, Polynome rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Polynome cannot be null");
            }

            var count = Math.Max(lhs._coefficients.Length, rhs._coefficients.Length);
            var sum = new double[count];

            for (int i = 0; i < count; i++)
            {
                double x = 0, y = 0;
                if (i < lhs._coefficients.Length)
                {
                    x = lhs[i];
                }
                if (i < rhs._coefficients.Length)
                {
                    y = rhs[i];
                }
                sum[i] = x + y;
            }

            return new Polynome(sum);
        }

        public static Polynome Add(Polynome lhs, Polynome rhs)
        {
            return lhs + rhs;
        }

        public Polynome Add(Polynome polynome)
        {
            return this + polynome;
        }

        public static Polynome operator -(Polynome lhs, Polynome rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Polynome cannot be null");
            }

            var count = Math.Max(lhs._coefficients.Length, rhs._coefficients.Length);
            var subtraction = new double[count];

            for (int i = 0; i < count; i++)
            {
                double x = 0, y = 0;
                if (i < lhs._coefficients.Length)
                {
                    x = lhs[i];
                }
                if (i < rhs._coefficients.Length)
                {
                    y = rhs[i];
                }
                subtraction[i] = x - y;
            }

            return new Polynome(subtraction);
        }

        public static Polynome Subtract(Polynome lhs, Polynome rhs)
        {
            return lhs - rhs;
        }

        public Polynome Subtract(Polynome polynome)
        {
            return this - polynome;
        }

        public static Polynome operator *(Polynome lhs, Polynome rhs)
        {
            if (lhs == null || rhs == null)
            {
                throw new ArgumentNullException($"Polynome cannot be null");
            }

            var count = lhs._coefficients.Length + rhs._coefficients.Length - 1;
            var multiplication = new double[count];

            for (int i = 0; i < lhs._coefficients.Length; i++)
            {
                for (int j = 0; j < rhs._coefficients.Length; j++)
                {
                    multiplication[i + j] = lhs[i] * rhs[j];
                }
            }

            return new Polynome(multiplication);
        }

        public static Polynome Multiply(Polynome lhs, Polynome rhs)
        {
            return lhs * rhs;
        }

        public Polynome Multiply(Polynome polynome)
        {
            return this * polynome;
        }

        public override string ToString()
        {
            const double eps = 1E-12;
            var sb = new StringBuilder();

            for (int i = 0; i < _coefficients.Length; i++)
            {
                if (Math.Abs(this[i]) < eps && i != 0)
                {
                    continue;
                }
                if (i == 0)
                {
                    sb.Append(this[i]);
                    continue;
                }
                if (this[i] > 0)
                {
                    sb.Append('+');
                }
                if (i == 1)
                {
                    sb.Append(this[i] + "x");
                    continue;
                }
                sb.Append(this[i] + "x^" + i);
            }

            return sb.ToString();
        }
    }
}
